
/**
 * Parser for one page (10 records) mindortrans.donland.ru
 * @author: Ivan Wong
 *
 * NOTES:
 * Now, i hate microsoft corporation and ASP NET technology
 */

var cheerio = require('cheerio'),
	Q = require("q"),
	request = require('request'),
	colors = require('colors'),
	csvWriter = require('csv-write-stream'),
	fs = require('fs');

// target address
var TARGET_URI = 'http://mindortrans.donland.ru/Default.aspx?pageid=103322';
// no comments. just fucking pagination token prototype
var FUCKING_PAGINATION_TOKEN = 'ctl01$mainContent$ctl00$carryPermissionsList$RadGrid1$ctl00$ctl03$ctl01$ctl';
// you can override any csv header if you want
var CSV_HEADERS = [
	'№ п/п', 'Номер', 'Перевозчик', 'Марка ТС', 'Модель ТС',
 	'Гос. номер', 'Срок действия', 'Дата выдачи', 'Дата прекращения', 'Приостановлено'
];
// result file
var CSV_RESULT_FILE = 'rostov20-15.csv';

var $ = cheerio;
var _form = {};
var _records = [];

/**
 * Create from from DOM inputs
 * @param  {cheerio dom object} elem DOM node for parse form inputs
 * @return {object} form
 */
function createForm(elem) {
	//console.log(elem);
	var form = {};
	elem.each(function(i, e) {
		var input = $(e);
		form[input.attr('name')] = input.attr('value');
	});

	return form;
}

/**
 * Comporator for the <sort> function
 * NOTE: Function need to improve for best sort effect
 */
function Comparator(a,b){
	if (a[1] < b[1]) return -1;
	if (a[1] > b[1]) return 1;
	return 0;
}


/**
 * Parse single page and save found records to array
 * @param  {object} form object to insert into request
 * @return {promise}
 */
function parsePage(form) {
	var defer = Q.defer();

	request.post({url: TARGET_URI, form: form}, function(err, res, body) {
		if(err) {
			defer.reject(err);
		}
		var $ = cheerio.load(body);

		var tr = $('.rgMasterTable > tbody > tr');
		tr.each(function(i, trow) {
			var recordEntity = [];

			$(trow).children().each(function(_i, td){
				recordEntity.push($(td).text());
			});

			_records.push(recordEntity);

		});

		//defer.resolve(number-1);
		defer.resolve();

	});
	return defer.promise;
}


/**
 * Init/create new form (will parsed from page)
 * @return {promise}
 */
function getForm() {

	var defer = Q.defer();

	request.post({url: TARGET_URI, form: {}}, function(err, res, body) {
		if(err) {
			defer.reject(err);
		}
		var $ = cheerio.load(body);
		defer.resolve(createForm($('#aspnetForm').children()));
	});



	return defer.promise;
}

/**
 * Generate pagination token for hack aspNET viewState
 * @param  {int} i iteration count
 * @return {string}   token for using as __EVENTTARGET in form
 */
function genToken(i) {
	if(i < 10) {
		return FUCKING_PAGINATION_TOKEN + '0' + i.toString();
	} else {
		return FUCKING_PAGINATION_TOKEN + i.toString();
	}
}

/**
 * Create array of promises for page parse
 * @param  {int} from iteration start position (used for token generation)
 * @param  {int} to  limit iteration (used for token generation)
 * @param  {object} form which contains all aspnet garbage that need for correct work
 * @return {object promise}
 */
function createQueue(from, to, form) {
	promisesQueue = [];
	for (var i = from; i <= to; i +=2) {
		var frm = form;
		frm.__EVENTTARGET = genToken(i);
		promisesQueue.push(parsePage(frm));
	}

	return promisesQueue;
}

/**
 * Write data array to result csv file
 * @param  {array} array for write
 */
function writeToCsv(data){
	var writer = csvWriter({
		separator: ',',
		newline: '\n',
		headers: CSV_HEADERS,
		sendHeaders: true
	});

	writer.pipe(fs.createWriteStream('./' + CSV_RESULT_FILE));

	data.forEach(function(entity) {
		writer.write(entity);

	});

	writer.end();
}

/**
 * ENTRY POINT
 */
function doParse() {
	var initPromise = Q.fcall(function() {
		return getForm();
	}, function(err) {
		console.error(err.red);
	});

	initPromise.then(function(form) {
		Q.all(createQueue(5, 23, form)).spread(function() {
			_records =_records.sort(Comparator);
			console.log('received records: '.blue, _records.length);
			console.log('successfully!'.green, 'saved to: '.green + CSV_RESULT_FILE.cyan);
			writeToCsv(_records);
		});
	}, function(err) {
		console.log('WARNING! Have failed requests'.red);
	});
}


// lets begin our sex and awesome brain fucking
doParse();
