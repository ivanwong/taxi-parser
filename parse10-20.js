var cheerio = require('cheerio'),
	Q = require("q"),
	request = require('request'),
	colors = require('colors'),
	csvWriter = require('csv-write-stream'),
	fs = require('fs');


var TARGET_URI = 'http://mindortrans.donland.ru/Default.aspx?pageid=103322';
var STD_ITEM_PER_PAGE = 10;
var FUCKING_PAGINATION_TOKEN = 'ctl01$mainContent$ctl00$carryPermissionsList$RadGrid1$ctl00$ctl03$ctl01$ctl';
var TOKEN_LEN = FUCKING_PAGINATION_TOKEN.length;
var CSV_HEADERS = [
	'№ п/п', 'Номер', 'Перевозчик', 'Марка ТС', 'Модель ТС',
 	'Гос. номер', 'Срок действия', 'Дата выдачи', 'Дата прекращения', 'Приостановлено'
];
var CSV_RESULT_FILE = 'rostov20-15.csv';

var $ = cheerio;
var _form = {};
var _records = [];

function createForm(elem) {
	//console.log(elem);
	var form = {};
	elem.each(function(i, e) {
		var input = $(e);
		form[input.attr('name')] = input.attr('value');
	});

	return form;
}

function Comparator(a,b){
	if (a[1] < b[1]) return -1;
	if (a[1] > b[1]) return 1;
	return 0;
}


function parsePage(form) {
	var defer = Q.defer();

	request.post({url: TARGET_URI, form: form}, function(err, res, body) {

		var $ = cheerio.load(body);

		var tr = $('.rgMasterTable > tbody > tr');
		tr.each(function(i, trow) {
			var recordEntity = [];

			$(trow).children().each(function(_i, td){
				recordEntity.push($(td).text());
			});

			_records.push(recordEntity);

		});

		//defer.resolve(number-1);
		defer.resolve();

	});
	return defer.promise;
}

function doParse() {

}

function getForm(form) {

	var defer = Q.defer();
	var frm = {};
	if(typeof form !== 'undefined') {
		form.__EVENTTARGET = genToken(25);
		frm = form;
	}

	request.post({url: TARGET_URI, form: frm}, function(err, res, body) {
		var $ = cheerio.load(body);
		defer.resolve(createForm($('#aspnetForm').children()));
	});



	return defer.promise;
}

function genToken(i) {
	if(i < 10) {
		return FUCKING_PAGINATION_TOKEN + '0' + i.toString();
	} else {
		return FUCKING_PAGINATION_TOKEN + i.toString();
	}
}

function createQueue(from, to, form) {
	promisesQueue = [];
	for (var i = from; i <= to; i +=2) {
		var frm = form;
		frm.__EVENTTARGET = genToken(i);
		promisesQueue.push(parsePage(frm));
	}

	return promisesQueue;
}

function writeToCsv(data){
	var writer = csvWriter({
		separator: ',',
		newline: '\n',
		headers: CSV_HEADERS,
		sendHeaders: true
	});

	writer.pipe(fs.createWriteStream('./' + CSV_RESULT_FILE));

	data.forEach(function(entity) {
		writer.write(entity);

	});

	writer.end();
}

function init() {
	var initPromise = Q.fcall(function() {
		return getForm();
	});

	initPromise.then(function(form) {

		reloadPromise = Q.fcall(function() {
			return getForm(form);
		});

		reloadPromise.then(function(reloadForm) {
			var queue = createQueue(5, 23, form);
			queue = queue.concat(createQueue(7, 25, reloadForm));
			Q.all(queue).spread(function() {
				_records =_records.sort(Comparator);
				writeToCsv(_records);
			});
		});
	});
}
init();
